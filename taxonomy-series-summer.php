<?php get_header(); ?>
hello piyas
<main id="content">

  <ng-controller data-ng-controller="AdController as adc" data-audience="summer" data-ng-cloak>

    <section class="clearfix has-cards hero--small">

      <div class="clearfix wrap" data-ng-cloak>

        <ng-repeat data-ng-repeat="ad in adc.ads| limitTo: 3" data-ng-cloak>
          <div  data-ng-if="$index > 0">
            <h2 class="align-center delta no-margin hero--small separator separator--top">{{ ad.title }}</h2>

            <a class="link link--undecorated" ng-href="{{ ad.link }}?utm_source=pls&utm_medium=card&utm_campaign=ad-manager">
              <article class="card">
                <div class="card__media">
                  <img ng-src="{{ad.media}}">
                </div>
                <section class="card__content">
                  <p class="zeta">{{ ad.excerpt }}</p>
                </section>
              </article>
            </a>
          </div>

        </ng-repeat>

      </div><!--/.wrap-->
    </section>
  </ng-controller>

	<div class="has-cards hero">

		<div class="wrap clearfix">

				<aside class="col-md--sixcol col-lg--fourcol">

					<?php if ( get_query_var( 'for' ) === 'kids' ) : ?>
					<div class="card">
						<div class="card__media">
							<a href="http://sherman.library.nova.edu/sites/spotlight/lists/?for=kids">
								<img src="http://sherman.library.nova.edu/sites/spotlight/files/2016/04/I-Aint-Gonna-Paint-No-More-e1462195646595.jpg" alt="An illustration of a girl painting her face">
							</a>
						</div>
						<div class="card__header clearfix">
							<a class="link link--undecorated _link-blue" href="http://sherman.library.nova.edu/sites/spotlight/lists/?for=kids">
								<h2 class="menu__item__title no-margin">Our Favorite Children's Books</h2>
							</a>
						</div>
					</div>

					<?php elseif ( get_query_var( 'for' ) === 'teens' ) : ?>
						<div class="card">
							<div class="card__media">
								<a href="http://sherman.library.nova.edu/sites/spotlight/lists/?for=teens">
									<img src="http://sherman.library.nova.edu/sites/spotlight/files/2016/03/warm_bodies-wide-e1462197968197.jpg" alt="Warm Bodies wallpaper">
								</a>
							</div>
							<div class="card__header clearfix">
								<a class="link link--undecorated _link-blue" href="http://sherman.library.nova.edu/sites/spotlight/lists/?for=teens">
									<h2 class="menu__item__title no-margin">YA Movie and Book Lists</h2>
								</a>
							</div>
						</div>

					<?php else : ?>
						<div class="card">
							<div class="card__media">
								<a href="http://sherman.library.nova.edu/sites/spotlight/lists/">
									<img src="http://sherman.library.nova.edu/sites/spotlight/files/2016/03/Across-The-Universe-Desktop-Wallpapers-across-the-universe-trilogy-30111464-1920-1200-e1462198543645.jpg" alt="Movie poster for Across the Universe">
								</a>
							</div>
							<div class="card__header clearfix">
								<a class="link link--undecorated _link-blue" href="http://sherman.library.nova.edu/sites/spotlight/lists/">
									<h2 class="menu__item__title no-margin">We Make Movie and Book Lists</h2>
								</a>
							</div>
						</div>
					<?php endif; ?>

					<div class="card">
						<div class="card__media">
							<a href="http://public.library.nova.edu/card/?utm_source=summer-website&utm_medium=card&utm_campaign=Summer%20Card%20Drive">
								<img src="http://public.library.nova.edu/wp-content/uploads/2013/10/library-card-e1461006529894.jpg" alt="Alvin Sherman Library card">
							</a>
						</div>
						<div class="card__header clearfix">
								<h2 class="menu__item__title no-margin" style="float: left; position: relative; top: 3px;"><span style="color:#e2624f;">♥</span> Your Library</h2>
								<a href="http://public.library.nova.edu/card/?utm_source=summer-website&utm_medium=card&utm_campaign=Summer%20Card%20Drive" class="button button--flat button--small green no-margin small-text" style="float: right;">Get a Card</a>
						</div>
					</div>

				</aside>

		    <section class="col-md--sixcol col-lg--eightcol clearfix">

		    	<?php get_template_part( 'loop', 'event-card' ); ?>

				</section> <!-- end #main -->
			</div>
	</div>
</main> <!-- end #content -->

<?php get_footer(); ?>
