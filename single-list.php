<?php if ( get_query_var( 'json' ) ) :

header( 'Content-type: application/json' );
header('Access-Control-Allow-Origin: http://public.library.nova.edu' );

if (have_posts()) : while (have_posts()) : the_post();

  if ( have_rows('asl_list_items') ): while ( have_rows('asl_list_items') ) : the_row();

    $items[] = array(
      'title' => get_sub_field( 'asl_item_title' ),
      'author' => get_sub_field( 'asl_item_author' ),
      'isbn' => get_sub_field( 'asl_item_isbn' ),
      'upc' => get_sub_field( 'asl_item_upc' ),
      'summary' => get_sub_field( 'asl_item_summary' ),
      'record' => get_sub_field( 'asl_item_permalink' )
    );

  endwhile; endif;

    $itemList[] = array(

      'title' => get_the_title(),
      'author' => get_the_author(),
      'excerpt' => get_the_excerpt(),
      'url' => get_permalink(),
      'items' => $items

    );

endwhile; endif;
echo json_encode( $itemList ); ?>

<?php else : ?>

<?php get_header(); ?>
<div id="content">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php
			$books = get_list( wp_get_attachment_url( get_post_meta( get_the_ID(), 'list_file', true) ) );
			$counter = 0;

			$checkoutVerbs = array(
				'Borrow'
			);

		?>

	<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">


			<header class="has-background background-base hero">

				<div class="wrap">

					<h1 class="col-lg--eightcol col--centered beta" itemprop="headline"><?php the_title(); ?></h1>

				</div><!--/.wrap-->

			</header>

			<section class="post-content clearfix hero wrap" itemprop="articleBody">

				<div class="col-lg--eightcol col--centered">

					<div class="byline clearfix zeta hero--small" style="border-bottom: 1px solid #ddd;">
						<div class="col-md--sixcol">
							<em>Picked by</em> <strong style="text-transform: uppercase;"><?php echo get_the_author(); ?></strong>
						</div>

						<div class="col-md--fourcol align-right" style="color: #aaa;">
			   				<?php printf(__('<time class="updated no-margin" datetime="%1$s" pubdate>%2$s</time>', 'bonestheme'), get_the_time('Y-m-j'), get_the_time(get_option('date_format')) );	?>
			   			</div>

			   			<div class="col-md--twocol">

							<div class="align-right share no-margin">

								<a class="link" onClick="_gaq.push(['_trackEvent', 'Shares', 'Click', 'Facebook']);" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Share on Facebook" target="new"><svg class="svg svg--facebook"><use xlink:href="#icon-facebook"></use></svg></a>

								<a class="link" onClick="_gaq.push(['_trackEvent', 'Shares', 'Click', 'Twitter']);" href="https://twitter.com/intent/tweet?text=<?php the_permalink(); ?>" title="Share on Twitter" target="new">
									<svg class="svg svg--twitter"><use xlink:href="#icon-twitter"></use></svg>
								</a>

							</div>

			   			</div>
					</div><!--/.byline-->


				<?php the_field( 'asl_list_lede' ); ?>

				<?php if ( have_rows('asl_list_items') ): ?>
					<ul class="list list--books" style="list-style: none; padding: 0;">

					<?php
				 	// loop through the rows of data
				    while ( have_rows('asl_list_items') ) : the_row();

				    $asl_item_type = get_sub_field( 'asl_item_type' );
				    $asl_syndetics_type = ( get_sub_field( 'asl_item_isbn' ) ? 'isbn' : 'upc' );
				    $asl_syndetics_slug = ( $asl_syndetics_type === 'isbn' ? get_sub_field( 'asl_item_isbn') : get_sub_field( 'asl_item_upc' ) );

				    ?>

						<li class="clearfix">

					        <div class="col-md--threecol media">

					            <a class="record-link" href="<?php the_sub_field('asl_item_permalink') ?>" onClick="ga( 'send', 'event', 'Catalog Items', 'Click - Cover', '<?php the_sub_field('asl_item_title') ?>' );">
					                <img src="//www.syndetics.com/index.php?<?php echo $asl_syndetics_type ?>=<?php echo $asl_syndetics_slug ?>/lc.gif&client=novaseu"
					                     srcset="//www.syndetics.com/index.php?<?php echo $asl_syndetics_type ?>=<?php echo $asl_syndetics_slug ?>/lc.gif&client=novaseu 1024w,
					                             //www.syndetics.com/index.php?<?php echo $asl_syndetics_type ?>=<?php echo $asl_syndetics_slug ?>/mc.gif&client=novaseu 720w,
					                             //www.syndetics.com/index.php?<?php echo $asl_syndetics_type ?>=<?php echo $asl_syndetics_slug ?>/sc.gif&client=novaseu 320w"
					                     alt="" />
					            </a>
					        </div>
					          <div class="col-md--ninecol">
					            <a class="link link--undecorated card__title delta no-margin" href="<?php the_sub_field('asl_item_permalink') ?>" onClick="ga( 'send', 'event', 'Catalog Items', 'Click - Title', '<?php the_sub_field('asl_item_title') ?>' );"><?php the_sub_field('asl_item_title') ?></a> <br>
					            <span class="zeta"> <a class="link link--undecorated" href="//novacat.nova.edu/search/X?SEARCH=a:(<?php the_sub_field('asl_item_author') ?>)&amp;searchscope=13"><?php the_sub_field('asl_item_author') ?></a></span>
					            <p><?php the_sub_field('asl_item_summary')?></p>
					        </div>
				        </li>

					   <?php endwhile; else : // no rows found ?>
					</ul>
				<?php endif; ?>
				<?php the_content(); ?>

				<ul class="list list--alternate">

					<?php

					foreach( $books as $book ) :

					// Identifying Information
					$bibrecord  = substr($book[0], 0, -1);
					$oclc 		= trim( $book[1], '' );
					$isbn       = trim($book[5],': ');
					if ( preg_match("/\s/", $isbn ) ) {
						$isbn 		= substr( $isbn, 0, strrpos( $isbn, ' ' ) );
					}


					// Title
					$title      = $book[2]; //str_replace( array('/'), '', $book[2]); //
					$subtitle   = $book[3]; //str_replace( array('/'), '', $book[3]);

					// Author
					$author     = $book[4];

					// Supplmentary Info
					$location 	= $book[6];
					$summary    = str_replace('"', '', $book[ count($book) - 2 ]); //$book[7]; //str_replace('"', '', $book[7]);

					//echo $summary;

					/**
					 * CLEANUP
					 * If the title and subtitle are identical, then we can guess that
					 * fields 245|a and 245|b failed to export and only 245 exported.
					 * Additionally, if that's the case, let's strip the crap off the default
					 * 245 field.
					 */

					if ( $title === $subtitle ) :

						// Get rid of subtitle.
						$subtitle 	= NULL;

						// Remove non-title supplementary information from 245
						$title 		= explode( ';', $title );
						$title 		= current( $title );
						$title 		= explode( '/', $title );
						$title 		= current( $title );

						// Remove brackets, "videorecording".
						//$title 		= str_replace( array( '[', ']', 'videorecording' ), '', $title );
						//$title      = str_replace( ' : ', ': ', $title);

						/**
						 * Remove Author Info from Default 245
						 */

						// Remove birth/death date
						$author = preg_replace("/(17|18|19|20)[0-9][0-9]/", '', $author);
						$author = preg_replace("/\([^)]+\)/", '', $author);
						$author = str_replace( array(', -', ', author'), '', $author);

						// Detect if the Author is present in the title
						$names = explode( ',', $author);
						$surname 	= current($names);
						$first 		= end($names);

						// If the author's name isn't an abbreviation, strip out the period
						if ( str_word_count( $first ) <= 2 ) :
							$first 	= rtrim( $first, '.' );
						endif;

						// Knock everything off the title after the surname
						//$title = substr( $title, 0, strpos( $title, $surname ) );

						// If author has just one name, then nullify the surname
						if ( $first === $surname ) {
							$surname = '';
						}


						// Locate and knock everything off after the first name.
						// This got complicated because the name appended to the title
						// is not always identical to the name entered as author.
						//$title = str_replace( array( 'by' . $first . ',', $first, str_replace(' ', '', $first)), '', $title );
						//$title = str_replace( 'by', '', $title );
						//$title = rtrim( $title, ' .' );

					endif; // ( if $title === $subtitle )


					?>

					<?php if ( $bibrecord && $oclc && $author) : ?>

					<li class="clearfix" style="border: 1px solid #e9e9e9; margin-bottom: 1em;">

						<div class="threecol first">
							<a class="record-link" href="//novacat.nova.edu/record=<?php echo $bibrecord; ?>">
								<img src="//www.syndetics.com/index.php?isbn=<?php echo $isbn; ?>/lc.gif&client=novaseu&oclc=<?php echo $oclc; ?>"
									 srcset="//www.syndetics.com/index.php?isbn=<?php echo $isbn; ?>/lc.gif&client=novaseu&oclc=<?php echo $oclc; ?> 1024w,
									 		 //www.syndetics.com/index.php?isbn=<?php echo $isbn; ?>/mc.gif&client=novaseu&oclc=<?php echo $oclc; ?> 720w,
									 		 //www.syndetics.com/index.php?isbn=<?php echo $isbn; ?>/sc.gif&client=novaseu&oclc=<?php echo $oclc; ?> 320w"
							 		 alt="">
							</a>

						</div>

						<div class="ninecol last">
							<a class="link link--undecorated card__title delta no-margin" href="//novacat.nova.edu/record=<?php echo $bibrecord; ?>~S13" title="<?php echo $title . ( !$subtitle ? '' : ': ' . $subtitle ); ?>">
								<?php echo $title . ( !$subtitle ? '' : ': ' . $subtitle ); ?>
							</a>

							<?php if ( $author ) : ?>
								<br> <span class="zeta no-margin">by <a class="link link--undecorated" href="//novacat.nova.edu/search/X?SEARCH=a:(<?php echo $author; ?>)&amp;searchscope=13" title="Find More from <?php echo $author; ?>"><?php echo $first . ' ' . $surname; ?></a></span>
							<?php else : ?>
							<?php endif; ?>
							<p class="zeta"><?php echo $summary; ?></p>

							<span class="hide-text"><?php echo $isbn; ?></span>

						</div>


				</li>
			<?php endif; ?>
				<?php endforeach; ?>
				</ul>

				</div>


			</section> <!-- end article section -->


			</article> <!-- end article -->

			<?php endwhile; ?>

			<?php else : ?>

				<article id="post-not-found" class="hentry clearfix">
		    		<header class="article-header">
		    			<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
		    		</header>
		    		<section class="post-content">
		    			<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
		    		</section>
		    		<footer class="article-footer">
		    		    <p><?php _e("This is the error message in the single.php template.", "bonestheme"); ?></p>
		    		</footer>
				</article>

			<?php endif; ?>
</div> <!-- end #content -->

<?php get_footer(); ?>
<?php endif; ?>

<?php

/* ==================
 * Book Lists
 * ================== */

// Each line will be accessed by it's position in the array
// $readfile[0] would be the first line because the array begins at 0
// rather than 1


// Create a loop that will read all elements of the array and print out
// each field of the tab-delimited text file
function get_list( $file ) {
$readfile = file($file);
$books = array();
    for ($k=1; $k<=count($readfile)-1; $k++) {
        $fields = explode("\t",$readfile[$k]);
       // echo count($fields) . '<br />';
        array_push( $books, $fields);
      }

    return $books;
}


 ?>
